include_guard(GLOBAL)

set(project_dir "${CMAKE_CURRENT_LIST_DIR}/..")
file(GLOB project_modules ${project_dir}/projects/*)
list(
  APPEND
  CMAKE_MODULE_PATH
  ${project_dir}/seminix
  ${project_dir}/seminix/cmake/helpers
  ${project_modules}
)
